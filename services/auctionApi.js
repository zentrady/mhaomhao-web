import HttpRequest from './http_request'

export default class AuctionAPI extends HttpRequest {
  getFruitTypes (auth) {
    this.setHeaderAuth(auth)
    return this.fetch('/auction/fruit_types/')
  }
  getUserFarms (auth) {
    this.setHeaderAuth(auth)
    return this.fetch('/auction/user/farms/')
  }
  getFarms (params, auth) {
    this.setHeaderAuth(auth)
    return this.fetch('/auction/farms/', params)
  }
  getFarmStandards (auth) {
    this.setHeaderAuth(auth)
    return this.fetch('/auction/farm_standard/')
  }
  getFarmById (id, auth) {
    this.setHeaderAuth(auth)
    return this.fetch(`/auction/farms/${id}/`)
  }
  getUserAuctionById (id, auth) {
    this.setHeaderAuth(auth)
    return this.fetch(`/auction/auction_base/${id}/`)
  }
  postUserAuction (body, auth) {
    this.setHeaderAuth(auth)
    return this.create('/auction/auction_base/', body)
  }
  putUserAuction (id, body, auth) {
    this.setHeaderAuth(auth)
    return this.update(`/auction/auction_base/${id}/`, body)
  }
  deleteAuction (id, auth) {
    this.setHeaderAuth(auth)
    return this.remove(`/auction/auction_base/${id}/`)
  }
  postBidAuction (body, auth) {
    this.setHeaderAuth(auth)
    return this.create('/auction/auction_transaction/', body)
  }
  postUserAuctionMedia (id, body, auth) {
    this.setHeaderAuth(auth)
    return this.createMultipart(`/auction/auction_base_medias/`, body)
  }
  deleteUserAuctionMedia (id, auth) {
    this.setHeaderAuth(auth)
    return this.remove(`/auction/auction_base_medias/${id}/`)
  }
  postUserAuctionOtp (body, auth) {
    this.setHeaderAuth(auth)
    return this.create(`/auction/auction_base_otp/`, body)
  }
  postBidOtp (id, auth) {
    this.setHeaderAuth(auth)
    return this.create(`/auction/auction_transaction_otp/`)
  }
  putUserAuctionOtp (id, body, auth) {
    this.setHeaderAuth(auth)
    return this.update(`/auction/user/auctions/${id}/otp/`, body)
  }
  postUserFarm (body, auth) {
    this.setHeaderAuth(auth)
    return this.create('/auction/user/farms/', body)
  }
  postUserFarmMedia (id, body, auth) {
    this.setHeaderAuth(auth)
    return this.createMultipart(`/auction/user/farms/${id}/medias/`, body)
  }
  deleteUserFarmMedia (id, mediaId, auth) {
    this.setHeaderAuth(auth)
    return this.remove(`/auction/user/farms/${id}/medias/${mediaId}/`)
  }
  putUserFarm (id, body, auth) {
    this.setHeaderAuth(auth)
    return this.update(`/auction/user/farms/${id}/`, body)
  }
  getAuctions (params, auth) {
    this.setHeaderAuth(auth)
    return this.fetch('/auction/auction_base/', params)
  }
  getAuctionById (id, auth) {
    this.setHeaderAuth(auth)
    return this.fetch(`/auction/auction_base/${id}`)
  }
  getSystemDailySummary (auth) {
    this.setHeaderAuth(auth)
    return this.fetch(`/auction/system_daily_summary/`)
  }
  getAuctionMedias (params, auth) {
    this.setHeaderAuth(auth)
    return this.fetch('/auction/auction_base_medias/', params)
  }
  getAuctionTopfive (params, auth) {
    this.setHeaderAuth(auth)
    return this.fetch('/auction/auction_transaction/', params)
  }
  postAuctionFavorite (id, auth) {
    this.setHeaderAuth(auth)
    return this.create(`/auction/auctions/favorite/${id}/`)
  }
  deleteAuctionFavorite (id, auth) {
    this.setHeaderAuth(auth)
    return this.remove(`/auction/auctions/favorite/${id}/`)
  }
}
