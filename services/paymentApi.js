import HttpRequest from './http_request'

export default class PaymentAPI extends HttpRequest {
  postPaymentSlip (body, auth) {
    this.setHeaderAuth(auth)
    return this.createMultipart(`/payment/`, body)
  }
}
