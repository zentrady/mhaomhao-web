import { make } from 'vuex-pathify'
import { AuctionService } from '../services'

const mapData = value => value.data
const mapPage = value => value.page

export const state = () => ({
  fruitTypes: [],
  farms: [],
  farmsPage: {},
  farmStandards: [],
  userFarms: [],
  userFarmsPage: {},
  userAuction: {},
  farmData: {},
  biddingAuction: {},
  auctions: [],
  auctionPageData: {},
  createFarmResult: {},
  createUserAuctionResult: {},
  createUserAuctionOtpResult: {},
  createBidAuctionResult: {},
  createBidAuctionOtpResult: {},
  topfiveAuction: [],
  userAuctionsCount: 0,
  systemDailySummary: {}
})

export const actions = {
  setBiddingAuction ({ commit }, auction) {
    commit('SET_BIDDING_AUCTION', auction)
  },
  async fetchFruitTypes ({ rootState: { auth: { token } }, commit }) {
    try {
      const result = await AuctionService.getFruitTypes(token)
      commit('SET_FRUIT_TYPES', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchFarms ({ rootState: { auth: { token } }, commit }, params) {
    try {
      const result = await AuctionService.getFarms(params, token)
      commit('SET_FARMS', mapData(result))
      commit('SET_FARMS_PAGE', mapPage(result))
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchFarmStandards ({ rootState: { auth: { token } }, commit }) {
    try {
      const result = await AuctionService.getFarmStandards(token)
      commit('SET_FARM_STANDARDS', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchUserFarms ({ rootState: { auth: { token } }, commit }) {
    try {
      const result = await AuctionService.getUserFarms(token)
      commit('SET_USER_FARMS', mapData(result))
      commit('SET_USER_FARMS_PAGE', mapPage(result))
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchFarmById ({ rootState: { auth: { token } }, commit }, id) {
    try {
      const result = await AuctionService.getFarmById(id, token)
      commit('SET_FARM_DATA', mapData(result)[0])
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchUserAuctionById ({ rootState: { auth: { token } }, commit }, id) {
    try {
      const result = await AuctionService.getUserAuctionById(id, token)
      commit('SET_USER_AUCTION', result)
      commit('SET_BIDDING_AUCTION', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchSystemDailySummary ({ rootState: { auth: { token } }, commit }) {
    try {
      const result = await AuctionService.getSystemDailySummary(token)
      commit('SET_SYSTEM_DAILY_SUMMARY', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async createUserAuction ({ rootState: { auth: { token } }, commit }, data) {
    try {
      const result = await AuctionService.postUserAuction(data, token)
      commit('SET_CREATE_USER_AUCTION_RESULT', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async updateUserAuction ({ rootState: { auth: { token } }, commit }, { id, data }) {
    try {
      const result = await AuctionService.putUserAuction(id, data, token)
      commit('SET_CREATE_USER_AUCTION_RESULT', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async removeAuction ({ rootState: { auth: { token } } }, id) {
    try {
      await AuctionService.deleteAuction(id, token)
      return Promise.resolve()
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async createUserAuctionMedia ({ rootState: { auth: { token } } }, { id, formData }) {
    try {
      await AuctionService.postUserAuctionMedia(id, formData, token)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async removeUserAuctionMedia ({ rootState: { auth: { token } } }, { mediaId }) {
    try {
      await AuctionService.deleteUserAuctionMedia(mediaId, token)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async createUserAuctionOtp ({ rootState: { auth: { token } }, commit }, data) {
    try {
      const result = await AuctionService.postUserAuctionOtp(data, token)
      commit('SET_CREATE_USER_AUCTION_OTP_RESULT', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async createBidAuction ({ rootState: { auth: { token } }, commit }, data) {
    try {
      const result = await AuctionService.postBidAuction(data, token)
      commit('SET_CREATE_BID_AUCTION_RESULT', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async createBidOtp ({ rootState: { auth: { token } }, commit }, id) {
    try {
      const result = await AuctionService.postBidOtp(id, token)
      commit('SET_CREATE_BID_AUCTION_OTP_RESULT', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async updateUserAuctionOtp ({ rootState: { auth: { token } } }, { id, data }) {
    try {
      await AuctionService.putUserAuctionOtp(id, data, token)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async createFarm ({ rootState: { auth: { token } }, commit }, data) {
    try {
      const result = await AuctionService.postUserFarm(data, token)
      commit('SET_CREATE_FARM_RESULT', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async createUserFarmMedia ({ rootState: { auth: { token } } }, { id, formData }) {
    try {
      await AuctionService.postUserFarmMedia(id, formData, token)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async removeUserFarmMedia ({ rootState: { auth: { token } } }, { id, mediaId }) {
    try {
      await AuctionService.deleteUserFarmMedia(id, mediaId, token)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async updateUserFarm ({ rootState: { auth: { token } } }, { id, formData }) {
    try {
      await AuctionService.putUserFarm(id, formData, token)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchAuctions ({ rootState: { auth: { token } }, commit }, params) {
    try {
      const result = await AuctionService.getAuctions(params, token)
      commit('SET_AUCTIONS', result.results)
      const { results, ...pageData } = result
      commit('SET_AUCTION_PAGE_DATA', pageData)
      return Promise.resolve(result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async createAuctionFavorite ({ rootState: { auth: { token } } }, id) {
    try {
      await AuctionService.postAuctionFavorite(id, token)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async removeAuctionFavorite ({ rootState: { auth: { token } } }, id) {
    try {
      await AuctionService.deleteAuctionFavorite(id, token)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchAuctionMedias ({ rootState: { auth: { token } }, commit }, params) {
    try {
      const result = await AuctionService.getAuctionMedias(params, token)
      return Promise.resolve(result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchAuctionTopfive ({ rootState: { auth: { token } }, commit }, params) {
    try {
      const resp = await AuctionService.getAuctionTopfive(params, token)
      const { results } = resp
      const topFive = results.slice(0, 5)
      commit('SET_TOPFIVE_AUCTION', topFive)
      return Promise.resolve(results)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchUserAuctionsCount ({ rootState: { auth: { token } }, commit }, params) {
    try {
      const result = await AuctionService.getAuctions(params, token)
      commit('SET_USER_AUCTIONS_COUNT', result.count)
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

export const mutations = make.mutations(state)
