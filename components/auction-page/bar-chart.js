import { Bar } from 'vue-chartjs'

export default {
  name: 'AuctionPageBarChart',
  extends: Bar,
  props: {
    labels: Array,
    dataList: Array
  },
  data () {
    return {
      backgroundColor: 'rgba(255, 167, 38, 1)'
    }
  },
  mounted () {
    this.renderChart({
      labels: this.labels,
      datasets: [
        {
          label: 'Data One',
          backgroundColor: this.backgroundColor,
          data: this.dataList
        }
      ]
    }, { responsive: true, maintainAspectRatio: false })
  }
}
